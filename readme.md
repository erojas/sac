# creacion de proyecto con laravel
En linux y mac `composer create-project --prefer-dist laravel/laravel blog`
En windows ` php d:\xampp\php\composer.phar create-project --prefer-dist laravel/laravel SAC`
# instalacion de  paquetes
En linux y mac `php composer install`
En consola, en la raiz del proyecto ejecutar `php d:\xampp\php\composer.phar install`
# Configuracion de bases de datos
Para la configuracion de la bases de datos, en el editor favorito habra el archivo `config/database.php` seleccione su bases de preferencia que es el postgres.
# creacionde bases de bases datos
En su administrador de bases de datos favorito crea una base de datos
# Ejecutar migraciones
En consola, en la raiz del proyecto ejecutar `php artisan:migrate`
# generate key application
copiar el contenido del archivo .env.example a .env y en consola ejecutar `php artisan key:generate`
# Inicializando los datos
En consola, en la raiz del proyecto ejecutar `php artisan db:seed`
# Ejecutando la aplicacion
En consola, en la raiz del proyecto ejecutar `php artisan serve`